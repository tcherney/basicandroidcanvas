package com.example.brickbreaker;

import com.example.brickbreaker.R.drawable;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
	
	private final int FPS = 60;
	private Thread canvasThread;
	private SurfaceHolder holder;
	private boolean isRunning;
	private Paint textPaint;
	private int width;
	private int height;
	private Bitmap playerBitmap;
	private Rect playerSrc;
	private Rect playerDst;
	private int playerBitmapIndex = 0;
	private int playerAnimationTime = 0;
	public GameView(Context context) {
		super(context);
		getHolder().addCallback(this);
		holder = getHolder();
		isRunning = false;
		textPaint = new Paint();
		textPaint.setTextSize(24);
		textPaint.setColor(Color.WHITE);
		width = height = 0;
		playerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.blueguy0);
		playerSrc = new Rect(0,0,32,32);
		playerDst = new Rect(0,0,32,32);
		playerDst.set(0,0,320, 320);
		canvasThread = new Thread(new Runnable(){

			@Override
			public void run() {
				long delta = 0;
				long timeToWait = 1000/FPS;
				while(isRunning) {
					Canvas c = null;
					long startTime = System.currentTimeMillis();
					try {
						c = holder.lockCanvas();
						synchronized (holder) {
							// Draw here
							if (c != null) {
								update(delta);
								draw(delta,c);
							}
							
						}
					} finally {
						if (c != null) {
							holder.unlockCanvasAndPost(c);
						}
					}
					long endTime = System.currentTimeMillis();
					delta = endTime-startTime;
					try {
						if (delta < timeToWait) {
							Thread.sleep(timeToWait-delta);
						}
					} catch (Exception e) {}
					
				}
			}
			
		});
	}
	@Override
	public boolean onTouchEvent(MotionEvent e){
		playerDst.set((int)e.getX(),((int)e.getY()),(int)e.getX()+320,((int)e.getY()+320));
		return true;
	}

	private void draw(long delta,Canvas c) {
		c.drawColor(Color.BLACK);
		c.drawBitmap(playerBitmap, playerSrc, playerDst, null);
	}
	
	private void update(long delta) {
		playerAnimationTime += delta;
		if (playerAnimationTime >= 100) {
			playerBitmapIndex = (playerBitmapIndex + 1)%3;
			playerAnimationTime = 0;
			playerSrc.set(playerBitmapIndex*32,0,(playerBitmapIndex+1)*32,31);
		}
	}
	@Override 
	public void surfaceCreated(SurfaceHolder holder) {
		if (!isRunning) {
			isRunning = true;
			canvasThread.start();
		}
		
		
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if (isRunning) {
			isRunning = false;
			try {
				canvasThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}
